## Unoffical personal ports for venom linux

This repository contains ports for the Venom linux distribution.

### Quick setup guide

In order to use this repository directly from scratchpkg do the following:
1. Add the repository in the `/etc/scratchpkg.repo` configuration file by adding the line:

`/usr/ports/mobinmob     https://codeberg.org/mobinmob/venom-ports`

2. Create the destination directory for the repo contents (as root):

`# mkdir -p /usr/ports/mobinmob`

3. You are ready! After `scratch sync` you can use the new repo.
